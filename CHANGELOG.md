
## 0.0.4 [02-09-2020]

* Bug fixes and performance improvements

See commit 44a21eb

---

## 0.0.3 [02-09-2020]

* Bug fixes and performance improvements

See commit 7fb62f8

---

## 0.0.2 [02-09-2020]

* Bug fixes and performance improvements

See commit 017e4df

---
